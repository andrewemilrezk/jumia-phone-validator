package com.phone.validator.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CountryDataUtils {
	public static List<String> createCountriesList() {
		return new ArrayList<>(
				Arrays.asList(new String[] { "Cameroon", "Ethiopia", "Morocco", "Mozambique", "Uganda" }));

	}
}
