package com.phone.validator.utils;

import java.util.ArrayList;
import java.util.List;

import com.phone.validator.dto.CustomerPhonesDto;
import com.phone.validator.dto.State;

public class CustomerDataUtils {
	public static List<CustomerPhonesDto> createAllPhoneNumbers() {
		List<CustomerPhonesDto> result = new ArrayList<>();
		result.add(new CustomerPhonesDto("Morocco", "212", State.VALID, "(212) 691933626"));
		result.add(new CustomerPhonesDto("Morocco", "212", State.INVALID, "(212) 6546545369"));
		result.add(new CustomerPhonesDto("Mozambique", "258", State.VALID, "(258) 847651504"));
		result.add(new CustomerPhonesDto("Mozambique", "258", State.INVALID, "(258) 84330678235"));
		result.add(new CustomerPhonesDto("Uganda", "256", State.VALID, "(256) 775069443"));
		result.add(new CustomerPhonesDto("Uganda", "256", State.INVALID, "(256) 7503O6263"));
		result.add(new CustomerPhonesDto("Ethiopia", "251", State.VALID, "(251) 914701723"));
		result.add(new CustomerPhonesDto("Ethiopia", "251", State.INVALID, "(251) 9119454961"));
		result.add(new CustomerPhonesDto("Cameroon", "237", State.VALID, "(237) 677046616"));
		result.add(new CustomerPhonesDto("Cameroon", "237", State.INVALID, "(237) 6A0311634"));
		return result;
	}

	public static List<Object[]> createAllPhoneNumbersAsObjects() {
		List<Object[]> result = new ArrayList<>();
		result.add(new Object[] { "Morocco", "212", State.VALID, "(212) 691933626" });
		result.add(new Object[] { "Morocco", "212", State.INVALID, "(212) 6546545369" });
		result.add(new Object[] { "Mozambique", "258", State.VALID, "(258) 847651504" });
		result.add(new Object[] { "Mozambique", "258", State.INVALID, "(258) 84330678235" });
		result.add(new Object[] { "Uganda", "256", State.VALID, "(256) 775069443" });
		result.add(new Object[] { "Uganda", "256", State.INVALID, "(256) 7503O6263" });
		result.add(new Object[] { "Ethiopia", "251", State.VALID, "(251) 914701723" });
		result.add(new Object[] { "Ethiopia", "251", State.INVALID, "(251) 9119454961" });
		result.add(new Object[] { "Cameroon", "237", State.VALID, "(237) 677046616" });
		result.add(new Object[] { "Cameroon", "237", State.INVALID, "(237) 6A0311634" });
		return result;
	}

	public static List<CustomerPhonesDto> createAllPhoneNumbersWithNullValues() {
		List<CustomerPhonesDto> result = new ArrayList<>();
		result.add(new CustomerPhonesDto("Morocco", "212", State.VALID, "(212) 691933626"));
		result.add(new CustomerPhonesDto("Morocco", "212", State.INVALID, "(212) 6546545369"));
		result.add(new CustomerPhonesDto("Mozambique", "", State.VALID, "(258) 847651504"));
		result.add(new CustomerPhonesDto("Mozambique", "258", State.INVALID, "(258) 84330678235"));
		result.add(new CustomerPhonesDto("Uganda", "256", State.VALID, "(256) 775069443"));
		result.add(new CustomerPhonesDto("Uganda", "256", State.INVALID, "(256) 7503O6263"));
		result.add(new CustomerPhonesDto("Ethiopia", "251", State.VALID, "(251) 914701723"));
		result.add(new CustomerPhonesDto("", "251", State.INVALID, "(251) 9119454961"));
		result.add(new CustomerPhonesDto("Cameroon", "237", State.VALID, "(237) 677046616"));
		result.add(new CustomerPhonesDto("Cameroon", "237", State.INVALID, "(237) 6A0311634"));
		return result;
	}

	public static List<Object[]> createAllPhoneNumbersWithNullValuesAsObjects() {
		List<Object[]> result = new ArrayList<>();
		result.add(new Object[] { "Morocco", "212", State.VALID, "(212) 691933626" });
		result.add(new Object[] { "Morocco", "212", State.INVALID, "(212) 6546545369" });
		result.add(new Object[] { "Mozambique", null, State.VALID, "(258) 847651504" });
		result.add(new Object[] { "Mozambique", "258", State.INVALID, "(258) 84330678235" });
		result.add(new Object[] { "Uganda", "256", State.VALID, "(256) 775069443" });
		result.add(new Object[] { "Uganda", "256", State.INVALID, "(256) 7503O6263" });
		result.add(new Object[] { "Ethiopia", "251", State.VALID, "(251) 914701723" });
		result.add(new Object[] { null, "251", State.INVALID, "(251) 9119454961" });
		result.add(new Object[] { "Cameroon", "237", State.VALID, "(237) 677046616" });
		result.add(new Object[] { "Cameroon", "237", State.INVALID, "(237) 6A0311634" });
		return result;
	}

	public static List<CustomerPhonesDto> createValidPhoneNumbers() {
		List<CustomerPhonesDto> result = new ArrayList<>();
		result.add(new CustomerPhonesDto("Morocco", "212", State.VALID, "(212) 691933626"));
		result.add(new CustomerPhonesDto("Mozambique", "258", State.VALID, "(258) 847651504"));
		result.add(new CustomerPhonesDto("Uganda", "256", State.VALID, "(256) 775069443"));
		result.add(new CustomerPhonesDto("Ethiopia", "251", State.VALID, "(251) 914701723"));
		result.add(new CustomerPhonesDto("Cameroon", "237", State.VALID, "(237) 677046616"));
		return result;
	}

	public static List<Object[]> createValidPhoneNumbersAsObjects() {
		List<Object[]> result = new ArrayList<>();
		result.add(new Object[] { "Morocco", "212", State.VALID, "(212) 691933626" });
		result.add(new Object[] { "Mozambique", "258", State.VALID, "(258) 847651504" });
		result.add(new Object[] { "Uganda", "256", State.VALID, "(256) 775069443" });
		result.add(new Object[] { "Ethiopia", "251", State.VALID, "(251) 914701723" });
		result.add(new Object[] { "Cameroon", "237", State.VALID, "(237) 677046616" });
		return result;
	}

	public static List<CustomerPhonesDto> createInvalidPhoneNumbers() {
		List<CustomerPhonesDto> result = new ArrayList<>();
		result.add(new CustomerPhonesDto("Morocco", "212", State.INVALID, "(212) 6546545369"));
		result.add(new CustomerPhonesDto("Mozambique", "258", State.INVALID, "(258) 84330678235"));
		result.add(new CustomerPhonesDto("Uganda", "256", State.INVALID, "(256) 7503O6263"));
		result.add(new CustomerPhonesDto("Ethiopia", "251", State.INVALID, "(251) 9119454961"));
		result.add(new CustomerPhonesDto("Cameroon", "237", State.INVALID, "(237) 6A0311634"));
		return result;
	}

	public static List<Object[]> createInvalidPhoneNumbersAsObjects() {
		List<Object[]> result = new ArrayList<>();
		result.add(new Object[] { "Morocco", "212", State.INVALID, "(212) 6546545369" });
		result.add(new Object[] { "Mozambique", "258", State.INVALID, "(258) 84330678235" });
		result.add(new Object[] { "Uganda", "256", State.INVALID, "(256) 7503O6263" });
		result.add(new Object[] { "Ethiopia", "251", State.INVALID, "(251) 9119454961" });
		result.add(new Object[] { "Cameroon", "237", State.INVALID, "(237) 6A0311634" });
		return result;
	}

	public static List<CustomerPhonesDto> createPhoneNumbersWithCountryFilter() {
		List<CustomerPhonesDto> result = new ArrayList<>();
		result.add(new CustomerPhonesDto("Uganda", "256", State.VALID, "(256) 775069443"));
		result.add(new CustomerPhonesDto("Uganda", "256", State.INVALID, "(256) 7503O6263"));
		return result;
	}

	public static List<Object[]> createPhoneNumbersWithCountryFilterAsObjects() {
		List<Object[]> result = new ArrayList<>();
		result.add(new Object[] { "Uganda", "256", State.VALID, "(256) 775069443" });
		result.add(new Object[] { "Uganda", "256", State.INVALID, "(256) 7503O6263" });
		return result;
	}

	public static List<CustomerPhonesDto> createPhoneNumbersWithBothFilters() {
		List<CustomerPhonesDto> result = new ArrayList<>();
		result.add(new CustomerPhonesDto("Uganda", "256", State.VALID, "(256) 775069443"));
		return result;
	}

	public static List<Object[]> createPhoneNumbersWithBothFiltersAsObjects() {
		List<Object[]> result = new ArrayList<>();
		result.add(new Object[] { "Uganda", "256", State.VALID, "(256) 775069443" });
		return result;
	}

}
