package com.phone.validator.country;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.phone.validator.repository.CountryRepository;
import com.phone.validator.utils.CountryDataUtils;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class CountryRepositoryTest {

	@Autowired
	CountryRepository countryRepo;

	@Test
	public void shouldReturnAllCountries() {
		List<String> actualResult = countryRepo.findAllCountryNames();
		List<String> expected = CountryDataUtils.createCountriesList();
		assertTrue(actualResult.size() == expected.size() && actualResult.containsAll(expected)
				&& expected.containsAll(actualResult));
	}

}
