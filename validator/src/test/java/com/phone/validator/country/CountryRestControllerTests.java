package com.phone.validator.country;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.phone.validator.controller.CountryRestController;
import com.phone.validator.service.CountryService;
import com.phone.validator.utils.CountryDataUtils;

@WebMvcTest(CountryRestController.class)
public class CountryRestControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CountryService countryService;

	@Autowired
	ObjectMapper objectMapper;

	@Test
	public void shouldReturnOKStatusForCorrectURL() throws Exception {
		List<String> countries = CountryDataUtils.createCountriesList();
		Mockito.when(countryService.findAllCountryNames()).thenReturn(countries);
		String url = "/country/findAllCountryNames";
		mockMvc.perform(get(url)).andExpect(status().isOk());
	}

	@Test
	public void shouldReturnNotFoundStatusForWrongURL() throws Exception {
		List<String> countries = CountryDataUtils.createCountriesList();
		Mockito.when(countryService.findAllCountryNames()).thenReturn(countries);
		String url = "/country/findAllCountries";
		mockMvc.perform(get(url)).andExpect(status().isNotFound());
	}

	@Test
	public void shouldReturnCorrectJsonResponse() throws Exception {
		List<String> countries = CountryDataUtils.createCountriesList();
		Mockito.when(countryService.findAllCountryNames()).thenReturn(countries);
		String url = "/country/findAllCountryNames";
		MvcResult mvcResult = mockMvc.perform(get(url)).andExpect(status().isOk()).andReturn();
		String actualJsonResponse = mvcResult.getResponse().getContentAsString();
		String expectedJsonResponse = objectMapper.writeValueAsString(countries);
		assertThat(actualJsonResponse).isEqualTo(expectedJsonResponse);

	}

}
