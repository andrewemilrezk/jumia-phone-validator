package com.phone.validator.country;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.phone.validator.repository.CountryRepository;
import com.phone.validator.service.impl.CountryServiceImpl;
import com.phone.validator.utils.CountryDataUtils;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class CountryServiceTests {
	@MockBean
	private CountryRepository countryRepo;

	@InjectMocks
	private CountryServiceImpl countryService;

	@Test
	public void shouldReturnAllCountries() {
		Mockito.when(countryRepo.findAllCountryNames()).thenReturn(CountryDataUtils.createCountriesList());
		List<String> actualResult = countryService.findAllCountryNames();
		assertEquals(actualResult, CountryDataUtils.createCountriesList());
	}
}
