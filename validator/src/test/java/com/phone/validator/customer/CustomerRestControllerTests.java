package com.phone.validator.customer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.phone.validator.controller.CustomerRestController;
import com.phone.validator.dto.CustomerPhonesDto;
import com.phone.validator.service.CustomerService;
import com.phone.validator.utils.CustomerDataUtils;

@WebMvcTest(CustomerRestController.class)
public class CustomerRestControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CustomerService customerService;

	@Autowired
	ObjectMapper objectMapper;

	@Test
	public void shouldReturnOKStatusForCorrectURL() throws Exception {
		String url = "/customer/findNumbers?countryName=Cameroon&state=VALID";
		mockMvc.perform(get(url)).andExpect(status().isOk());
	}

	@Test
	public void shouldReturnNotFoundStatusForWrongURL() throws Exception {
		String url = "/customer/findPhones";
		mockMvc.perform(get(url)).andExpect(status().isNotFound());
	}

	@Test
	public void shouldReturnBadRequestStatusForInvalidParamValue() throws Exception {
		String url = "/customer/findNumbers?countryName=ALL&state=BOTH";
		mockMvc.perform(get(url)).andExpect(status().isBadRequest());
	}

	@Test
	public void shouldReturnBadRequestStatusForMissingParamValue() throws Exception {
		String url = "/customer/findNumbers?state=VALID";
		mockMvc.perform(get(url)).andExpect(status().isBadRequest());
	}

	@Test
	public void shouldReturnCorrectJsonResponse() throws Exception {
		List<CustomerPhonesDto> customerPhones = CustomerDataUtils.createAllPhoneNumbers();
		Mockito.when(customerService.findCutomerPhones("ALL", "ALL")).thenReturn(customerPhones);
		String url = "/customer/findNumbers?countryName=ALL&state=ALL";
		MvcResult mvcResult = mockMvc.perform(get(url)).andExpect(status().isOk()).andReturn();
		String actualJsonResponse = mvcResult.getResponse().getContentAsString();
		String expectedJsonResponse = objectMapper.writeValueAsString(customerPhones);
		assertThat(actualJsonResponse).isEqualTo(expectedJsonResponse);

	}

}
