package com.phone.validator.customer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.phone.validator.dto.CustomerPhonesDto;
import com.phone.validator.repository.CustomerRepository;
import com.phone.validator.service.impl.CustomerServiceImpl;
import com.phone.validator.utils.CustomerDataUtils;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class CustomerServiceTests {

	@MockBean
	private CustomerRepository customerRepo;

	@InjectMocks
	private CustomerServiceImpl customerService;

	@Test
	public void shouldGetAllPhoneNumbersUsingALLFilter() {
		String countryName = "ALL";
		String state = "ALL";
		Mockito.when(customerRepo.findAllPhoneNumbers()).thenReturn(CustomerDataUtils.createAllPhoneNumbersAsObjects());
		Mockito.when(customerRepo.findPhoneNumbersByCountryAndState(countryName, state)).thenReturn(new ArrayList<>());
		Mockito.when(customerRepo.findPhoneNumbersByCountry(countryName)).thenReturn(new ArrayList<>());
		Mockito.when(customerRepo.findPhoneNumbersByState(state)).thenReturn(new ArrayList<>());

		List<CustomerPhonesDto> actualResult = customerService.findCutomerPhones(countryName, state);
		assertEquals(actualResult, CustomerDataUtils.createAllPhoneNumbers());
	}

	@Test
	public void shouldReturnOnlyFilteredCountryInfoWhenFilteringByCountry() {
		String countryName = "Uganda";
		String state = "ALL";
		Mockito.when(customerRepo.findAllPhoneNumbers()).thenReturn(CustomerDataUtils.createAllPhoneNumbersAsObjects());
		Mockito.when(customerRepo.findPhoneNumbersByCountryAndState(countryName, state)).thenReturn(new ArrayList<>());
		Mockito.when(customerRepo.findPhoneNumbersByCountry(countryName))
				.thenReturn(CustomerDataUtils.createPhoneNumbersWithCountryFilterAsObjects());
		Mockito.when(customerRepo.findPhoneNumbersByState(state)).thenReturn(new ArrayList<>());

		List<CustomerPhonesDto> actualResult = customerService.findCutomerPhones(countryName, state);
		assertEquals(actualResult, CustomerDataUtils.createPhoneNumbersWithCountryFilter());
	}

	@Test
	public void shouldReturnOnlyFilteredStateInfoWhenFilteringByState() {
		String countryName = "ALL";
		String state = "INVALID";
		Mockito.when(customerRepo.findAllPhoneNumbers()).thenReturn(CustomerDataUtils.createAllPhoneNumbersAsObjects());
		Mockito.when(customerRepo.findPhoneNumbersByCountryAndState(countryName, state)).thenReturn(new ArrayList<>());
		Mockito.when(customerRepo.findPhoneNumbersByCountry(countryName)).thenReturn(new ArrayList<>());
		Mockito.when(customerRepo.findPhoneNumbersByState(state))
				.thenReturn(CustomerDataUtils.createInvalidPhoneNumbersAsObjects());

		List<CustomerPhonesDto> actualResult = customerService.findCutomerPhones(countryName, state);
		assertEquals(actualResult, CustomerDataUtils.createInvalidPhoneNumbers());
	}

	@Test
	public void shouldReturnInfoAfterApplyingBothCountryAndStateFilters() {
		String countryName = "Uganda";
		String state = "VALID";
		Mockito.when(customerRepo.findAllPhoneNumbers()).thenReturn(CustomerDataUtils.createAllPhoneNumbersAsObjects());
		Mockito.when(customerRepo.findPhoneNumbersByCountryAndState(countryName, state))
				.thenReturn(CustomerDataUtils.createPhoneNumbersWithBothFiltersAsObjects());
		Mockito.when(customerRepo.findPhoneNumbersByCountry(countryName))
				.thenReturn(CustomerDataUtils.createPhoneNumbersWithCountryFilterAsObjects());
		Mockito.when(customerRepo.findPhoneNumbersByState(state))
				.thenReturn(CustomerDataUtils.createValidPhoneNumbersAsObjects());

		List<CustomerPhonesDto> actualResult = customerService.findCutomerPhones(countryName, state);
		assertEquals(actualResult, CustomerDataUtils.createPhoneNumbersWithBothFilters());
	}

	@Test
	public void shouldReplaceNullsWithEmptyString() {
		String countryName = "ALL";
		String state = "ALL";
		Mockito.when(customerRepo.findAllPhoneNumbers())
				.thenReturn(CustomerDataUtils.createAllPhoneNumbersWithNullValuesAsObjects());
		Mockito.when(customerRepo.findPhoneNumbersByCountryAndState(countryName, state)).thenReturn(new ArrayList<>());
		Mockito.when(customerRepo.findPhoneNumbersByCountry(countryName)).thenReturn(new ArrayList<>());
		Mockito.when(customerRepo.findPhoneNumbersByState(state)).thenReturn(new ArrayList<>());

		List<CustomerPhonesDto> actualResult = customerService.findCutomerPhones(countryName, state);
		assertEquals(actualResult, CustomerDataUtils.createAllPhoneNumbersWithNullValues());
	}

}
