package com.phone.validator.customer;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.phone.validator.repository.CustomerRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class CustomerRepositoryTests {

	@Autowired
	private CustomerRepository customerRepo;

	@Test
	public void shouldReturnAllPhoneNumbersWhenNoFilters() {
		List<Object[]> actualResult = customerRepo.findAllPhoneNumbers();
		assertTrue(actualResult.size() == 41);
	}

	@Test
	public void shouldReturnPhoneNumbersFilteredByCountry() {
		List<Object[]> actualResult = customerRepo.findPhoneNumbersByCountry("Cameroon");
		assertTrue(actualResult.size() == 10);
	}

	@Test
	public void shouldReturnPhoneNumbersFilteredByState() {
		List<Object[]> actualResult = customerRepo.findPhoneNumbersByState("VALID");
		assertTrue(actualResult.size() == 27);
	}

	@Test
	public void shouldReturnPhoneNumbersFilteredByCountryAndState() {
		List<Object[]> actualResult = customerRepo.findPhoneNumbersByCountryAndState("Uganda", "INVALID");
		assertTrue(actualResult.size() == 4);
	}

}
