package constatns;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class CustomerPhonesConstants {
	public static final String ALL_KEYWORD = "ALL";
	public static final String VALID = "VALID";
	public static final String INVALID = "INVALID";
	public static final Set<String> AVAILABLE_STATUS = new HashSet<>(Arrays.asList(ALL_KEYWORD, VALID, INVALID));

}
