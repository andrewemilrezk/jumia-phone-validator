package com.phone.validator.dto;

public class CustomerPhonesDto {
	private String countryName;
	private State state;
	private String countryCode;

	public CustomerPhonesDto(String countryName, String countryCode, State state, String phoneNumber) {
		super();
		this.countryName = countryName;
		this.state = state;
		this.countryCode = countryCode;
		this.phoneNumber = phoneNumber;
	}

	private String phoneNumber;

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public boolean equals(Object other) {
		CustomerPhonesDto otherObject = (CustomerPhonesDto) other;
		return this.countryName.equals(otherObject.countryName) && this.countryCode.equals(otherObject.countryCode)
				&& this.state.equals(otherObject.state) && this.phoneNumber.equals(otherObject.phoneNumber);
	}
}
