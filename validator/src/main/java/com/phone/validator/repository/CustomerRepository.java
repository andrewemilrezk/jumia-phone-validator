package com.phone.validator.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.phone.validator.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

	@Query(value = "SELECT country.name, country.code, IF(customer.phone REGEXP country.phone_regex, 'VALID', 'INVALID' ) as state, customer.phone FROM customer left join country on customer.phone LIKE CONCAT ('(', country.code, ')', '%') where country.name = :countryName having state = :state", nativeQuery = true)
	List<Object[]> findPhoneNumbersByCountryAndState(@Param("countryName") String countryName,
			@Param("state") String state);

	@Query(value = "SELECT country.name, country.code, IF(customer.phone REGEXP country.phone_regex, 'VALID', 'INVALID' ) as state, customer.phone FROM customer left join country on customer.phone LIKE CONCAT ('(', country.code, ')', '%') having state = :state", nativeQuery = true)
	List<Object[]> findPhoneNumbersByState(@Param("state") String state);

	@Query(value = "SELECT country.name, country.code, IF(customer.phone REGEXP country.phone_regex, 'VALID', 'INVALID') as state, customer.phone FROM customer left join country on customer.phone LIKE CONCAT ('(', country.code, ')', '%') where country.name = :countryName", nativeQuery = true)
	List<Object[]> findPhoneNumbersByCountry(@Param("countryName") String countryName);

	@Query(value = "SELECT country.name, country.code, IF(customer.phone REGEXP country.phone_regex, 'VALID', 'INVALID' ) as state, customer.phone FROM customer left join country on customer.phone LIKE CONCAT ('(', country.code, ')', '%')", nativeQuery = true)
	List<Object[]> findAllPhoneNumbers();

}
