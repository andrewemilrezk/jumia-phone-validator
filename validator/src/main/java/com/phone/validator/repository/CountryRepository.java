package com.phone.validator.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.phone.validator.model.Country;

public interface CountryRepository extends JpaRepository<Country, Integer> {
	@Query("SELECT country.name FROM Country country")
	List<String> findAllCountryNames();
}
