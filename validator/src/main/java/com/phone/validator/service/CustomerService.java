package com.phone.validator.service;

import java.util.List;

import com.phone.validator.dto.CustomerPhonesDto;

public interface CustomerService {
	List<CustomerPhonesDto> findCutomerPhones(String country, String status);

}
