package com.phone.validator.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.phone.validator.dto.CustomerPhonesDto;
import com.phone.validator.dto.State;
import com.phone.validator.repository.CustomerRepository;
import com.phone.validator.service.CustomerService;

import constatns.CustomerPhonesConstants;

@Service
public class CustomerServiceImpl implements CustomerService {
	@Autowired
	CustomerRepository customerRepo;

	@Override
	public List<CustomerPhonesDto> findCutomerPhones(String country, String status) {
		if (country == null || status == null)
			return new ArrayList<>();
		List<Object[]> result = new ArrayList<>();
		if (CustomerPhonesConstants.ALL_KEYWORD.equals(country) && CustomerPhonesConstants.ALL_KEYWORD.equals(status)) {
			result = customerRepo.findAllPhoneNumbers();
		} else if (CustomerPhonesConstants.ALL_KEYWORD.equals(country)
				|| CustomerPhonesConstants.ALL_KEYWORD.equals(status)) {
			result = CustomerPhonesConstants.ALL_KEYWORD.equals(country) ? customerRepo.findPhoneNumbersByState(status)
					: customerRepo.findPhoneNumbersByCountry(country);
		} else {
			result = customerRepo.findPhoneNumbersByCountryAndState(country, status);
		}
		return mapResultToCustomerPhonesDto(result);
	}

	private List<CustomerPhonesDto> mapResultToCustomerPhonesDto(List<Object[]> result) {
		List<CustomerPhonesDto> customerPhones = new ArrayList<>();
		if (!CollectionUtils.isEmpty(result)) {
			for (Object[] res : result) {
				customerPhones.add(new CustomerPhonesDto(res[0] == null ? "" : res[0].toString(),
						res[1] == null ? "" : res[1].toString(),
						res[2] == null ? State.INVALID : State.valueOf(res[2].toString()),
						res[3] == null ? "" : res[3].toString()));
			}
		}
		return customerPhones;
	}

}
