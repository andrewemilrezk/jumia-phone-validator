package com.phone.validator.service;

import java.util.List;

public interface CountryService {
	List<String> findAllCountryNames();
}
