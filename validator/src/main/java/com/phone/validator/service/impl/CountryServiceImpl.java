package com.phone.validator.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phone.validator.repository.CountryRepository;
import com.phone.validator.service.CountryService;

@Service
public class CountryServiceImpl implements CountryService {

	@Autowired
	private CountryRepository countryRepo;

	@Override
	public List<String> findAllCountryNames() {

		return countryRepo.findAllCountryNames();
	}

}
