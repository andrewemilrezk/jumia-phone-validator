package com.phone.validator.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.phone.validator.service.CountryService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/country")
public class CountryRestController {

	@Autowired
	CountryService countryService;

	@GetMapping("/findAllCountryNames")
	@ResponseBody
	public ResponseEntity<List<String>> findAllCountryNames() {
		return new ResponseEntity<>(countryService.findAllCountryNames(), HttpStatus.OK);
	}

}
