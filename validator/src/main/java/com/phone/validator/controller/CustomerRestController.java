package com.phone.validator.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.phone.validator.dto.CustomerPhonesDto;
import com.phone.validator.service.CustomerService;

import constatns.CustomerPhonesConstants;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/customer")
public class CustomerRestController {

	@Autowired
	CustomerService customerService;

	@GetMapping("/findNumbers")
	@ResponseBody
	public ResponseEntity<List<CustomerPhonesDto>> findAllCountries(String countryName,
			@RequestParam(required = true) String state) {
		if (state == null || countryName == null || !CustomerPhonesConstants.AVAILABLE_STATUS.contains(state))
			return new ResponseEntity<>(new ArrayList<>(), HttpStatus.BAD_REQUEST);
		return new ResponseEntity<>(customerService.findCutomerPhones(countryName, state), HttpStatus.OK);
	}

}
