# Start Guide

# Backend:
Navigate to jumia-phone-validator/validator directory then do the following:

## Create docker network through which spring-boot application and mySQL database will communicate
docker network create springboot-mysql-network

## Create docker container for mySQL inside the network
docker container run -d  --network springboot-mysql-network --name=mysql-docker --env="MYSQL_ROOT_PASSWORD=12345678"  --env="MYSQL_PASSWORD=12345678"  --env="MYSQL_DATABASE=phones" mysql:8

## Import customer.sql to the database on the container(this is the same as sample.db sent)
docker exec -i mysql-docker mysql -uroot -p12345678 phones < customer.sql

## Create image for spring-boot application
docker image build -t springboot-image .

## Run springboot container through the created image in the same network
docker container run --network springboot-mysql-network --name springboot-container -p 8080:8080 -d springboot-image


# Frontend:
Navigate to jumia-phone-validator/validator-client directory then do the following:

## Create docker image for react application
docker build . -t react-image

## run react container through the created image
docker container run -p 3000:3000 react-image

## Navigate to app url
http://localhost:3000/



# Technology Stack:
- spring-boot
- mySQL
- ReactJS
- docker


