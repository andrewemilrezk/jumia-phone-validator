import React from 'react';
import CustomerAPI from '../api/CustomerAPI';
import '../css/CustomerStyle.css';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';

const stateOptions = ['ALL', 'VALID', 'INVALID'];
class CustomerPage extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            countryName: 'ALL',
            state: 'ALL',
            customerData: [],
            countries: [],
        }
        this.getPhoneNumbers = this.getPhoneNumbers.bind(this);
        this.getAllCountryNames = this.getAllCountryNames.bind(this);
        this.renderTableComponent = this.renderTableComponent.bind(this);
        this.renderFilterComponent = this.renderFilterComponent.bind(this);
    }

    componentDidMount(){
        this.getPhoneNumbers();
        this.getAllCountryNames();
    }
    getPhoneNumbers(){
        CustomerAPI.getPhoneNumbers(this.state.countryName, this.state.state).then((response) =>{
            this.setState({customerData: response.data});
        });
    }
    getAllCountryNames(){
        CustomerAPI.getAllCountryNames().then((response) => {
            this.setState({countries: response.data});
        })
    }
    renderFilterComponent(){
        const countryOptions = ["ALL"].concat(this.state.countries);

        return (
                <div>
                    <div className='drop-down'>
                            <label className='label-left'>Filter Country</label>
                            <Dropdown
                                options={countryOptions}
                                onChange={(country) => this.setState({countryName: country.value}, () => {
                                    this.getPhoneNumbers();
                                })}
                                value={this.state.countryName || countryOptions[0]}
                                placeholder="Select Country"
                            />
                    </div>
                    <div className='drop-down-state'>
                            <label className='label-right'>Filter State</label>
                            <Dropdown 
                                options={stateOptions}
                                onChange={(state) => this.setState({state: state.value}, () => {
                                    this.getPhoneNumbers();
                                })}
                                value={this.state.countryNstateame || stateOptions[0]}
                                placeholder="Select State"
                            />   
                </div>
            </div>
    );
    }
    renderTableComponent(){
        return (
            <table>
                    <thead>
                        <tr>
                            <th> Country Name</th>
                            <th> State</th>
                            <th> Country Code</th>
                            <th> Phone Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.customerData.map((customer) => 
                                <tr key = {customer.phoneNumber}>
                                     <td> {customer.countryName}</td>   
                                     <td> {customer.state}</td>   
                                     <td> +{customer.countryCode}</td>   
                                     <td> {customer.phoneNumber}</td>   
                                </tr>
                            )
                        }

                    </tbody>
                </table>
        );
    }

        
    render(){
        return (
            <div>
                {this.renderFilterComponent()}
                {this.renderTableComponent()}
            </div>
        );
    }

}
export default CustomerPage;