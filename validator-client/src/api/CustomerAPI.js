import axios from 'axios';

const CUSTOMER_BASE_URL = 'http://localhost:8080';

class CustomerAPI {
    getPhoneNumbers(countryName, state){
        return axios.get(CUSTOMER_BASE_URL+'/customer/findNumbers',
         { params:
             { 
                countryName,
                state,
            },
         });
    }

    getAllCountryNames(){
        return axios.get(CUSTOMER_BASE_URL+'/country/findAllCountryNames');
    }
}

export default new CustomerAPI();