import './App.css';
import CustomerPage from './components/CustomerPage';

function App() {
  return (
    <div className="App">
      <h1 className = "App-header"> Customer Phone Numbers</h1>
      <CustomerPage />
    </div>
  );
}

export default App;
